package com.bzs2.Notes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class DeleteDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_dialog_message)
                .setPositiveButton(R.string.menu_option_delete_note, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((ViewActivity)getActivity()).
                                onDeleteDialogPositiveClick();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, null);
        return builder.create();
    }
}
