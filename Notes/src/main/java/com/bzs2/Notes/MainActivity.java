package com.bzs2.Notes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static class ViewHolder {
        TextView name, date;
    }

    private class ListItem {
        private String name, date;

        public ListItem(String name, String date) {
            this.name = name;
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public String getDate() {
            return date;
        }
    }

    private class CustomArrayAdapter extends ArrayAdapter<ListItem> {
        private ArrayList<ListItem> list;

        public CustomArrayAdapter(Context context, int textViewResId, ArrayList<ListItem> items) {
            super(context, textViewResId, items);
            this.list = new ArrayList<>();
            this.list.addAll(items);
        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder item = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(view == null) view = inflater.inflate(R.layout.list_item, null);
            item.name = (TextView) view.findViewById(R.id.list_item_text1);
            item.date = (TextView) view.findViewById(R.id.list_item_text2);

            item.name.setText(list.get(position).getName());
            item.date.setText(list.get(position).getDate());

            return view;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)actionBar.setIcon(R.drawable.ic_launcher);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ListView listView = (ListView) findViewById(R.id.id_list_view);
        listView.setDivider(null);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_list_item);
        LayoutAnimationController layoutAnimationController = new LayoutAnimationController(animation);
        layoutAnimationController.setDelay(0.1f);
        listView.setLayoutAnimation(layoutAnimationController);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] fileList = this.getFilesDir().list();
        ArrayList<ListItem> notes = new ArrayList<>();
        for (String s : fileList) {
            File file = new File(this.getFilesDir().getPath() + "/" + s);
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(file.lastModified()));
            notes.add(new ListItem(s, c.get(Calendar.HOUR_OF_DAY) + ":" +
                    String.format(Locale.ENGLISH, "%02d", c.get(Calendar.MINUTE)) + " " +
                    c.get(Calendar.DAY_OF_MONTH) + "/" +
                    c.get(Calendar.MONTH) + "/" +
                    c.get(Calendar.YEAR)));
        }

        ListView listView = (ListView) findViewById(R.id.id_list_view);
        CustomArrayAdapter adapter = new CustomArrayAdapter(this, R.id.list_item_text1, notes);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoViewActivity(((TextView) view.findViewById(R.id.list_item_text1)).getText().toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_option_clear) {
            DeleteAllDialogFragment dialog = new DeleteAllDialogFragment();
            dialog.show(this.getFragmentManager(), "delete_dialog");
            return true;
        }
        if(item.getItemId() == R.id.menu_new_note) {
            createNewNote();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createNewNote() {
        Intent intent = new Intent(this, EditActivity.class);
        startActivity(intent);
    }

    public void onDeleteAllDialogPositiveClick() {
        deleteAllNotes();
        this.onResume();
    }

    private void deleteAllNotes() {
        String[] fileList = this.getFilesDir().list();
        for (String str : fileList) {
            File file = new File(this.getFilesDir().getPath() + "/" + str);
            if (file.exists())
                if (!file.delete()) {
                    Toast.makeText(this, getString(R.string.file_delete_error), Toast.LENGTH_SHORT).show();
                }
        }
    }

    public void gotoViewActivity(String file) {
        Intent intent = new Intent(this, ViewActivity.class);
        if (file != null) intent.putExtra("file_name", file);
        startActivity(intent);
    }
}
