package com.bzs2.Notes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

final class FileIO {

    private FileIO() {}

    static String readFile(String file) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader fis = new BufferedReader(new InputStreamReader(
                    new FileInputStream(file)));

            String line;
            String separator = System.getProperty("line.separator");
            while ((line = fis.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(separator);
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    static int writeFile(String file, String body) {
        try {
            Writer fo = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));
            fo.write(body);
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }
}
