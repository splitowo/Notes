package com.bzs2.Notes;

import java.io.File;

import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {
    private String noteFilename = null;
    private EditText editText, editTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            //actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            //actionBar.setIcon(R.drawable.ic_launcher);
        }
        Intent intent = this.getIntent();
        if (intent.hasExtra("file_name")) noteFilename = intent.getStringExtra("file_name");
        editText = findViewById(R.id.edit_note);
        editTitle = findViewById(R.id.edit_title);

        if(savedInstanceState != null) {
            if (savedInstanceState.containsKey("title") && savedInstanceState.containsKey("note")) {
                editTitle.setText(savedInstanceState.getString("title"));
                editText.setText(savedInstanceState.getString("note"));
            }
        }
        else if (noteFilename != null) {
            editTitle.setText(noteFilename);
            editText.setText(FileIO.readFile(this.getFilesDir().getPath() + "/" + noteFilename));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("title", editTitle.getText().toString());
        outState.putString("note", editText.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_save_note) {
            saveNote();
            return true;
        }
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveNote() {
        if (noteFilename != null) {
            File file = new File(this.getFilesDir().getPath() + "/" + noteFilename);
            if (!file.delete()) {
                Toast.makeText(this, getString(R.string.file_delete_error), Toast.LENGTH_SHORT).show();
            }
        }

        int n = editText.getText().toString().length();
        if (n != 0) {
            String note_title = editTitle.getText().toString();
            if (note_title.length() == 0) {
                if (n > 15) {
                    n = 15;
                    note_title = editText.getText().toString().substring(0, n) + "...";
                } else note_title = editText.getText().toString().substring(0, n);
            }
            FileIO.writeFile(this.getFilesDir().getPath() + "/" + note_title,
                    editText.getText().toString());
            this.finish();
        } else {
            Toast.makeText(this, getString(R.string.toast_nothing_to_write), Toast.LENGTH_SHORT).show();
        }
    }
}
