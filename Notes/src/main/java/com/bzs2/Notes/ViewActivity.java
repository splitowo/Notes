package com.bzs2.Notes;

import java.io.File;

import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class ViewActivity extends AppCompatActivity {
    private String noteFilename = null;
    private TextView viewTitle, viewNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setIcon(R.drawable.ic_launcher);
        }
        Intent intent = this.getIntent();
        if (intent.hasExtra("file_name")) noteFilename = intent.getStringExtra("file_name");
        viewTitle = findViewById(R.id.view_title);
        viewNote = findViewById(R.id.view_note);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (noteFilename != null) {
            viewTitle.setText(noteFilename);
            String note = FileIO.readFile(this.getFilesDir().getPath() + "/" + noteFilename);
            if(note.equals(""))finish();
            viewNote.setText(note);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_activity_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_edit_note) {
            editNote();
            return true;
        }
        if(item.getItemId() == R.id.menu_delete_note) {
            DeleteDialogFragment dialog = new DeleteDialogFragment();
            dialog.show(this.getFragmentManager(), "delete_dialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void editNote() {
        Intent intent = new Intent(this, EditActivity.class);
        if (noteFilename != null) intent.putExtra("file_name", noteFilename);
        startActivity(intent);
    }

    public void onDeleteDialogPositiveClick() {
        deleteNote();
        this.finish();
    }

    private void deleteNote() {
        if (noteFilename != null) {
            File file = new File(this.getFilesDir().getPath() + "/" + noteFilename);
            if (!file.delete()) {
                Toast.makeText(this, getString(R.string.file_delete_error), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
