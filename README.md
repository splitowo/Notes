# Notes
A lightweight notes application for Android 4.0.0+.

Get an APK via
- [GitHub project releases] (https://github.com/splitowo/Notes/releases)
- [Google Play] (https://play.google.com/store/apps/details?id=com.bzs2.Notes)
